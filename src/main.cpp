#include <benchmark/benchmark.h>

#include <iostream>
#include <string>
#include <vulkan/vulkan.h>

class Instance final {
public:
  Instance();
  ~Instance();
  Instance(Instance const &) = delete;
  Instance &operator=(Instance const &) = delete;

  VkInstance instance() { return instance_; }
  VkPhysicalDevice physical_device() { return physical_device_; }

private:
  VkInstance instance_;
  VkPhysicalDevice physical_device_;
};

Instance::Instance() {
  const VkInstanceCreateInfo instance_create_info = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
  };
  if (vkCreateInstance(&instance_create_info, nullptr, &instance_) !=
      VK_SUCCESS) {
    std::cerr << "create instance failed\n";
    throw - 1;
  }

  std::vector<VkPhysicalDevice> physical_devices;
  std::uint32_t physical_device_count;

  if (vkEnumeratePhysicalDevices(instance_, &physical_device_count, nullptr) !=
      VK_SUCCESS) {
    std::cerr << "query physical devices count fail\n";
    vkDestroyInstance(instance_, nullptr);
    throw - 1;
  }

  if (physical_device_count == 0) {
    std::cerr << "no physical devices\n";
    vkDestroyInstance(instance_, nullptr);
    throw - 1;
  }

  physical_devices.resize(physical_device_count);
  if (vkEnumeratePhysicalDevices(instance_, &physical_device_count,
                                 physical_devices.data()) != VK_SUCCESS) {
    std::cerr << "query physical devices fail\n";
    vkDestroyInstance(instance_, nullptr);
    throw - 1;
  }
  if (physical_device_count == 0) {
    std::cerr << "found zero physical devices\n";
    throw - 1;
  }

  physical_device_ = physical_devices[0];
}

Instance::~Instance() { vkDestroyInstance(instance_, nullptr); }

Instance *get_instance() {
  static Instance *instance = new Instance();
  return instance;
}

class Device {
public:
  Device();
  ~Device();
  Device(Device const &) = delete;
  Device &operator=(Device const &) = delete;

  VkDevice operator*() { return device_; }

  VkQueue graphics_queue() { return queue_; }
  VkCommandPool graphics_queue_pool() { return cmd_pool_; }

private:
  Instance *instance_;
  VkDevice device_;
  VkQueue queue_;
  VkCommandPool cmd_pool_;
};

Device::Device() : instance_(get_instance()) {
  float queue_priority = 1.0;
  VkDeviceQueueCreateInfo queue_create_info = {
      VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      NULL,
      0,
      0,
      1,
      &queue_priority};
  VkDeviceCreateInfo device_create_info = {VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                                           NULL, 0, 1, &queue_create_info};

  if (vkCreateDevice(instance_->physical_device(), &device_create_info, nullptr,
                     &device_) != VK_SUCCESS) {
    std::cerr << "create device failed\n";
    throw - 1;
  }

  vkGetDeviceQueue(device_, 0, 0, &queue_);

  VkCommandPoolCreateInfo cmd_pool_create_info = {};
  cmd_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cmd_pool_create_info.queueFamilyIndex = 0;

  vkCreateCommandPool(device_, &cmd_pool_create_info, nullptr, &cmd_pool_);
}

Device::~Device() {
  vkDestroyCommandPool(device_, cmd_pool_, nullptr);
  vkDestroyDevice(device_, nullptr);
}

static void PlainFenceCreationDestruction(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  for (auto _ : state) {
    VkFence fence;

    auto result = vkCreateFence(*dev, &info, nullptr, &fence);
    assert(result == VK_SUCCESS);

    vkDestroyFence(*dev, fence, nullptr);
  }
}
BENCHMARK(PlainFenceCreationDestruction);

static void ExternalFenceCreationDestruction(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkExportFenceCreateInfo export_info = {};
  export_info.sType = VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO;
  export_info.handleTypes = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT;

  info.pNext = &export_info;
  for (auto _ : state) {
    VkFence fence;

    auto result = vkCreateFence(*dev, &info, nullptr, &fence);
    assert(result == VK_SUCCESS);

    vkDestroyFence(*dev, fence, nullptr);
  }
}
BENCHMARK(ExternalFenceCreationDestruction);

static void SubmitSingleCommandBuffer(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 1;

  VkCommandBuffer cmd_buf;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, &cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  vkBeginCommandBuffer(cmd_buf, &begin_info);
  vkEndCommandBuffer(cmd_buf);

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmd_buf;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 1, &cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitSingleCommandBuffer);

static void SubmitSingleCommandBuffer100xSimultUse(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 1;

  VkCommandBuffer cmd_buf;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, &cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

  vkBeginCommandBuffer(cmd_buf, &begin_info);
  vkEndCommandBuffer(cmd_buf);

  VkCommandBuffer cmd_bufs[100];
  for (int i = 0; i < 100; ++i)
	  cmd_bufs[i] = cmd_buf;
  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 100;
  submit_info.pCommandBuffers = cmd_bufs;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 1, &cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitSingleCommandBuffer100xSimultUse);

static void SubmitDifferentCommandBuffers100x(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 100;

  VkCommandBuffer cmd_buf[100];
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  for (unsigned i = 0; i < 100; ++i) {
    vkBeginCommandBuffer(cmd_buf[i], &begin_info);
    vkEndCommandBuffer(cmd_buf[i]);
  }

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 100;
  submit_info.pCommandBuffers = cmd_buf;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 100, cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitDifferentCommandBuffers100x);

static void SubmitDifferentCommandBuffers100xSimultUse(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 100;

  VkCommandBuffer cmd_buf[100];
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

  for (unsigned i = 0; i < 100; ++i) {
    vkBeginCommandBuffer(cmd_buf[i], &begin_info);
    vkEndCommandBuffer(cmd_buf[i]);
  }

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 100;
  submit_info.pCommandBuffers = cmd_buf;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 100, cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitDifferentCommandBuffers100xSimultUse);

static void SubmitSingleCommandBuffer100xSecondary(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 1;

  VkCommandBuffer cmd_buf;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, &cmd_buf);
  assert(result == VK_SUCCESS);

  cmd_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
  VkCommandBuffer sec_cmd_buf;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, &sec_cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  vkBeginCommandBuffer(sec_cmd_buf, &begin_info);
  vkEndCommandBuffer(sec_cmd_buf);

  vkBeginCommandBuffer(cmd_buf, &begin_info);
  for (int i = 0; i < 100; ++i) {
	  vkCmdExecuteCommands(cmd_buf, 1, &sec_cmd_buf);
  }
  vkEndCommandBuffer(cmd_buf);

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmd_buf;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 1, &sec_cmd_buf);
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 1, &cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitSingleCommandBuffer100xSecondary);

static void SubmitDifferentCommandBuffer100xSecondary(benchmark::State &state) {
  Device dev;
  VkFenceCreateInfo info = {};
  info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  VkFence fence;

  auto result = vkCreateFence(*dev, &info, nullptr, &fence);
  assert(result == VK_SUCCESS);

  VkCommandBufferAllocateInfo cmd_alloc_info = {};
  cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmd_alloc_info.commandPool = dev.graphics_queue_pool();
  cmd_alloc_info.commandBufferCount = 1;

  VkCommandBuffer cmd_buf;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, &cmd_buf);
  assert(result == VK_SUCCESS);

  cmd_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
  VkCommandBuffer sec_cmd_buf[100];
  cmd_alloc_info.commandBufferCount = 100;
  result = vkAllocateCommandBuffers(*dev, &cmd_alloc_info, sec_cmd_buf);
  assert(result == VK_SUCCESS);

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  for (int i = 0; i < 100; ++i) {
    vkBeginCommandBuffer(sec_cmd_buf[i], &begin_info);
    vkEndCommandBuffer(sec_cmd_buf[i]);
  }

  vkBeginCommandBuffer(cmd_buf, &begin_info);
  vkCmdExecuteCommands(cmd_buf, 100, sec_cmd_buf);
  vkEndCommandBuffer(cmd_buf);

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmd_buf;

  for (auto _ : state) {
    vkResetFences(*dev, 1, &fence);
    vkQueueSubmit(dev.graphics_queue(), 1, &submit_info, fence);
    vkQueueWaitIdle(dev.graphics_queue());
  }
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 100, sec_cmd_buf);
  vkFreeCommandBuffers(*dev, dev.graphics_queue_pool(), 1, &cmd_buf);
  vkDestroyFence(*dev, fence, nullptr);
}
BENCHMARK(SubmitDifferentCommandBuffer100xSecondary);


BENCHMARK_MAIN();
